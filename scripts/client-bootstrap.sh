#!/bin/bash

# Get and install puppetlabs repo
wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb
sudo dpkg -i puppetlabs-release-trusty.deb

# Update package cache
sudo apt-get update

# Install puppetmaster-passenger
sudo apt-get -y install puppet

# Update hosts file
echo "172.31.10.10 puppet" >> /etc/hosts

# Boostrap puppet agent
sudo puppet agent -t --waitforcert 60
