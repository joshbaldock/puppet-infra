#!/bin/bash

# Get and install puppetlabs repo
wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb
sudo dpkg -i puppetlabs-release-trusty.deb

# Update package cache
sudo apt-get update

# Install puppetmaster-passenger
sudo apt-get -y install puppetmaster-passenger

# Install puppetdb
sudo apt-get -y install puppetdb

# Install r10k
sudo gem install r10k

# Install git
sudo apt-get -y install git

# Deploy r10k environments
sudo r10k deploy environment -p -v -c /vagrant/r10k.yaml

# Update hosts file
sed -i 's/localhost$/puppet localhost/' /etc/hosts

# Enable directory environments in puppet
sudo puppet config set environmentpath "/etc/puppet/environments"

# Add hiera.yaml symlink
sudo ln -s /etc/puppet/environments/production/hiera.yaml /etc/puppet/hiera.yaml

# Bootstrap puppet master
sudo puppet apply -e "class{'puppet::agent': templatedir => undef,} \
		      class{'puppet::master': autosign => true, environments => 'directory',} \
		      class{'puppetdb': } class{'puppetdb::master::config': puppet_service_name => 'apache2', } "

# Boostrap puppet agent
sudo puppet agent -t
